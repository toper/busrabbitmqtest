﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pirios.Common.Messages
{
    public interface ISwitchOff
    {
        string Id { get; set; }
    }

    public class Message1 : ISwitchOff
    {

        public string Msg { get; set; }
        public DateTime MsgTime { get; set; }
        public string Id { get; set; }
    }

    public class SwitchOffChange : ISwitchOff
    {
        public string Id { get; set; }
        public int StateId { get; set; }
    }
}
