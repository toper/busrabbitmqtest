Solucja zawiera 4 projekty:

1. WebPublisher - jest to moduł który po wciskaniu Enteru - generuje Messege typu "Message1" na szynę BUS.

2. Module1 - to odpowiednik Informera - ten moduł w ilu by nie był odpalonych instancjach powinna zawsze tylko jedna z tych instancji dostawać wiadomość - na zasadzie loadbalancera

3. Module2 i Module3 - to jakieś inne moduły, które będą dostawać każdą wiadomość, którą wysłał WebPublisher



Pod adresem:  https://raven.rmq.cloudamqp.com/#/ - jest RabbitMq Manager - login i hasło są w App.Config każdego z modułów w appseting.
