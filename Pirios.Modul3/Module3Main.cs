﻿using EasyNetQ;
using EasyNetQ.AutoSubscribe;
using EasyNetQ.Logging;
using NLog;
using Pirios.Module3.BusConsumers;
using Pirios.Module3.Helper;
using Pirios.ModulesInstaller;
using SimpleInjector;
using System;
using System.Threading.Tasks;

namespace Pirios.Module3
{
    public class Module3Main : IPlugin
    {
        private static NLog.Logger _logger = LogManager.GetCurrentClassLogger();
        public static Container _container = new Container();
        IBus bus = null;
        public string Name => "Publisher2";

        public void InitPlugin()
        {
            _logger.Trace("PLUGIN: start");
            try
            {
                BootstrapperPackage.RegisterServices(_container);

                new Task(() =>
                {
                    _logger.Trace("Start polaczenie do RabbitMQ");

                    var rabbitMqConnection = _container.GetInstance<IConfigurationHelper>().BusRabittMqConnection + ";product=" + Name;
                    LogProvider.SetCurrentLogProvider(ConsoleLogProvider.Instance);
                    bus = RabbitHutch.CreateBus(rabbitMqConnection);

                    _logger.Trace("IBus polaczenie stworzone: " + bus.IsConnected);

                    var subscriber = new AutoSubscriber(bus, Name);
                    //subscriber.GenerateSubscriptionId = subscriptionInfo => { return subscriptionInfo.ConcreteType.Name; };

                    //subscriber.Subscribe(Assembly.GetExecutingAssembly());
                    subscriber.Subscribe(typeof(MessageConsumer));

                    Console.ReadLine();

                }).Start();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"PLUGIN: start, exception. Message: {ex.Message}");
            }
        }

        public void StopPlugin()
        {
            try
            {
                _logger.Trace("PLUGIN: stop");
                bus.Dispose();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"PLUGIN: stop, exception. Message: {ex.Message}");
            }
        }
    }
}
