﻿using System;

namespace Pirios.Module3.Helper
{
    public interface IConfigurationHelper
    {
        /// <summary>
        /// Connection for RabbittMq
        /// </summary>
        string BusRabittMqConnection { get; }
    }
}
