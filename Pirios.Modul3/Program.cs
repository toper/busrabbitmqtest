using Pirios.ModulesInstaller;
using System.Reflection;

namespace Pirios.Module3
{
    class Program : PiriosInstaller
    {
        static void Main(string[] args)
        {
            ServiceName = typeof(Program).Namespace;
            Assembly assembly = Assembly.GetExecutingAssembly();
            ProgramRunner.Run<HostServicesFactory, Module3Main>(assembly, ServiceName, args);
        }
    }
}
