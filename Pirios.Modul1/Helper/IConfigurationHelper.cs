﻿using System;

namespace Pirios.Module1.Helper
{
    public interface IConfigurationHelper
    {
        /// <summary>
        /// Connection for RabbittMq
        /// </summary>
        string BusRabittMqConnection { get; }
    }
}
