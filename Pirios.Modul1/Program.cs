using Pirios.ModulesInstaller;
using System.Reflection;

namespace Pirios.Module1
{
    class Program : PiriosInstaller
    {
        static void Main(string[] args)
        {
            ServiceName = typeof(Program).Namespace;
            Assembly assembly = Assembly.GetExecutingAssembly();
            ProgramRunner.Run<HostServicesFactory, Module1Main>(assembly, ServiceName, args);
        }
    }
}
