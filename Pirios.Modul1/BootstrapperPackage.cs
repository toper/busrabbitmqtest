﻿using System;
using System.Linq;
using NLog;
using Pirios.Module1.Helper;
using SimpleInjector;

namespace Pirios.Module1
{
    public static class BootstrapperPackage
    {
        public static void RegisterServices(Container container)
        {
            container.RegisterInstance<ILogger>(LogManager.GetCurrentClassLogger());

            container.Register<IConfigurationHelper, ConfigurationHelper>(Lifestyle.Singleton);

        }
    }
}
