﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyNetQ.AutoSubscribe;
using NLog;
using Pirios.Common.Messages;

namespace Pirios.Module1.BusConsumers
{
    public class MessageConsumer : IConsume<Message1>
    {
        private readonly ILogger logger;

        public MessageConsumer()
        {
            this.logger = LogManager.GetCurrentClassLogger();
        }

        //[AutoSubscriberConsumer(SubscriptionId = "Informer")]
        public void Consume(Message1 message)
        {
            logger.Info("Przyszło: Msg:" + message.Msg + " info:" + message.Id + " z czasem: " + message.MsgTime.ToUniversalTime());
        }
    }
}
