﻿using System;
using System.Linq;
using NLog;
using Pirios.WebPublisher.Helper;
using SimpleInjector;

namespace Pirios.WebPublisher
{
    public static class BootstrapperPackage
    {
        public static void RegisterServices(Container container)
        {
            container.RegisterInstance<ILogger>(LogManager.GetCurrentClassLogger());

            container.Register<IConfigurationHelper, ConfigurationHelper>(Lifestyle.Singleton);

        }
    }
}
