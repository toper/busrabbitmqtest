using Pirios.ModulesInstaller;
using System.Reflection;

namespace Pirios.WebPublisher
{
    class Program : PiriosInstaller
    {
        static void Main(string[] args)
        {
            ServiceName = typeof(Program).Namespace;
            Assembly assembly = Assembly.GetExecutingAssembly();
            ProgramRunner.Run<HostServicesFactory, WebPublisherMain>(assembly, ServiceName, args);
        }
    }
}
