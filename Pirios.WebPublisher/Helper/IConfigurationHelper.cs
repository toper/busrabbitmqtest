﻿using System;

namespace Pirios.WebPublisher.Helper
{
    public interface IConfigurationHelper
    {
        /// <summary>
        /// Connection for RabbittMq
        /// </summary>
        string BusRabittMqConnection { get; }

    }
}
