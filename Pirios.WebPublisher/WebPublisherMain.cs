﻿using EasyNetQ;
using NLog;
using Pirios.ModulesInstaller;
using Pirios.WebPublisher.Helper;
using SimpleInjector;
using System;
using System.Threading.Tasks;
using Pirios.Common.Messages;
using System.Threading;
using EasyNetQ.AutoSubscribe;
using System.Reflection;
using Pirios.WebPublisher.BusConsumers;

namespace Pirios.WebPublisher
{
    public class WebPublisherMain : IPlugin
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public static Container _container = new Container();

        IBus bus = null;

        public string Name => "WebPublisher";

        public void InitPlugin()
        {
            _logger.Info("PLUGIN: WebPublisher start - press q ENTER - to exit");
            try
            {
                BootstrapperPackage.RegisterServices(_container);

                new Task(() => 
                {
                    _logger.Trace("Start polaczenie do RabbitMQ");

                    var rabbitMqConnection = _container.GetInstance<IConfigurationHelper>().BusRabittMqConnection+ ";product=" + Name;

                    bus = RabbitHutch.CreateBus(rabbitMqConnection);

                    _logger.Trace("IBus polaczenie stworzone: " + bus.IsConnected);

                    var subscriber = new AutoSubscriber(bus, Name);
                    subscriber.Subscribe(Assembly.GetExecutingAssembly());

                    var input = "";
                    while ((input = Console.ReadLine()) != "q")
                    {
                        _logger.Debug("Wysylam wiadomość");
                        bus.Publish(new Message1
                        {
                            Id = input,
                            Msg = "Wiadomość od WebPublishera",
                            MsgTime = DateTime.Now
                        });
                    }

                }).Start();

            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"PLUGIN: start, exception. Message: {ex.Message}");
            }
        }

        public void StopPlugin()
        {
            try
            {
                bus.Dispose();
                _logger.Trace("PLUGIN: stop");
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"PLUGIN: stop, exception. Message: {ex.Message}");
            }
        }
    }
}
