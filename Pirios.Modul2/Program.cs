using Pirios.ModulesInstaller;
using System.Reflection;

namespace Pirios.Module2
{
    class Program : PiriosInstaller
    {
        static void Main(string[] args)
        {
            ServiceName = typeof(Program).Namespace;
            Assembly assembly = Assembly.GetExecutingAssembly();
            ProgramRunner.Run<HostServicesFactory, Module2Main>(assembly, ServiceName, args);
        }
    }
}
