﻿using System;
using System.Configuration;

namespace Pirios.Module2.Helper
{
    public class ConfigurationHelper : IConfigurationHelper
    {

        public ConfigurationHelper()
        {
            ConfigurationManager.RefreshSection("appSettings");
            BusRabittMqConnection = ConfigurationManager.AppSettings["BusRabittMqConnection"];
        }

        public string BusRabittMqConnection { get; }

    }
}
