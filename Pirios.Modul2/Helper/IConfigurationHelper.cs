﻿using System;

namespace Pirios.Module2.Helper
{
    public interface IConfigurationHelper
    {
        /// <summary>
        /// Connection for RabbittMq
        /// </summary>
        string BusRabittMqConnection { get; }
    }
}
