﻿using System;
using System.Linq;
using NLog;
using Pirios.Module2.Helper;
using SimpleInjector;

namespace Pirios.Module2
{
    public static class BootstrapperPackage
    {
        public static void RegisterServices(Container container)
        {
            container.RegisterInstance<ILogger>(LogManager.GetCurrentClassLogger());

            container.Register<IConfigurationHelper, ConfigurationHelper>(Lifestyle.Singleton);

        }
    }
}
